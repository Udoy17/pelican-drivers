//
//  FirstSignUpVC.swift
//  Pelican Driver
//
//  Created by iMac on 31/1/19.
//  Copyright © 2019 Silicon Orchard Ltd. All rights reserved.
//

import UIKit

class SignUpFirstStepVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var signUpTableView: UITableView!
    
    let data = ["One", "Two", "Three"]
    let SignUpFirstStepItems: [SignUpDataModel] =  SignUpDataModel.getItemsForFirstStep()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        registerTVCell()
        self.addGestureToHideKeyoardOnTappingAround()
    }
    
    //MARK: - SETUP
    func setupTableView() {
        signUpTableView.delegate    = self
        signUpTableView.dataSource  = self
        signUpTableView.tableFooterView = UIView(frame: .zero)
    }
    
    func registerTVCell() {
        signUpTableView.register(UINib(nibName: "SignUpSingleItemTVCell", bundle: nil), forCellReuseIdentifier: CellIdentifier.singleItemCell)
        signUpTableView.register(UINib(nibName: "SignUpDoubleItemTVCell", bundle: nil), forCellReuseIdentifier: CellIdentifier.doubleItemCell)
        signUpTableView.register(UINib(nibName: "SignUpPhotoItemTVCell", bundle: nil), forCellReuseIdentifier: CellIdentifier.photoItemCell)
        signUpTableView.register(UINib(nibName: "SignUpButtonItemTVCell", bundle: nil), forCellReuseIdentifier: CellIdentifier.buttonItemCell)
    }
    
    
    //MARK: - BUTTON ACTIONS
    @objc func nextButtonClicked(sender: UIButton) {
        self.view.endEditing(true)
        //Validation
        let signUpStoryboar = UIStoryboard(name: "SignUp", bundle: nil)
        let secondSignUpVC = signUpStoryboar.instantiateViewController(withIdentifier: "secondSignUpVC")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.pushViewController(secondSignUpVC, animated: true)
    }

    
    //MARK: - TABLEVIEW METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SignUpFirstStepItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dataItem = SignUpFirstStepItems[indexPath.row]
        switch dataItem.identifier{
        case CellIdentifier.singleItemCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.singleItemCell, for: indexPath) as! SignUpSingleItemTVCell
            cell.configCellWith(dataItem: dataItem)
            cell.inputTextField.delegate = self
            return cell
            
        case CellIdentifier.doubleItemCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.doubleItemCell, for: indexPath) as! SignUpDoubleItemTVCell
            cell.configCellWith(dataItem: dataItem)
            cell.firstTextField.delegate = self
            cell.secondTextField.delegate = self
            return cell
            
        case CellIdentifier.photoItemCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.photoItemCell, for: indexPath) as! SignUpPhotoItemTVCell
            cell.configCellWith(dataItem: dataItem)
            return cell
            
        case CellIdentifier.buttonItemCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.buttonItemCell, for: indexPath) as! SignUpButtonItemTVCell
            cell.configCellWith(dataItem: dataItem)
            cell.buttonWidthConstraint.constant = (cell.buttonWidthConstraint.constant/2)
            cell.centerXConstraint.constant = (self.view.frame.width-40)/2 - cell.buttonWidthConstraint.constant/2
            
            if !cell.nextSignUpButton.allTargets.contains(self) {
                cell.nextSignUpButton.addTarget(self, action: #selector(nextButtonClicked(sender:)), for: .touchUpInside)
            }
            return cell
            
        default:
            break
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == SignUpFirstStepItems.count - 1 {
            return 60
        }
        return 100
    }
    
    
    //MARK: - CUSTOM METHODS
    func addGestureToHideKeyoardOnTappingAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
}

extension SignUpFirstStepVC: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let buttonPosition: CGPoint = textField.convert(CGPoint.zero, to:self.signUpTableView)
        let indexPath = self.signUpTableView.indexPathForRow(at: buttonPosition)
        
        if let indexPath = indexPath {
            
            let model = self.SignUpFirstStepItems[indexPath.row]
            if model.identifier == CellIdentifier.doubleItemCell {
                let cell = signUpTableView.cellForRow(at: indexPath)
                if cell is SignUpDoubleItemTVCell {
                    //TODO: - handle multiple cell
                    model.items?[0].value = (cell as! SignUpDoubleItemTVCell).firstTextField.text
                    model.items?[1].value = (cell as! SignUpDoubleItemTVCell).secondTextField.text
                }
            } else {
                var item = model.items![0]
                item.value = textField.text ?? ""
                model.items = [item]
            }
        }
        self.signUpTableView.reloadData()
    }
}
