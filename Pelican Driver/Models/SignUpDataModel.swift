//
//  SignUpDataModel.swift
//  Pelican Driver
//
//  Created by iMac on 31/1/19.
//  Copyright © 2019 Silicon Orchard Ltd. All rights reserved.
//

import Foundation


enum InputFieldType: String {
    case firstName = "First Name"
    case lastName = "Last Name"
    case email = "Email"
    case password = "Password"
    case confirmPassword = "Confirm Password"
    case phoneNumber = "Phone Number"
    case dateOfBirth = "Date of Birth"
    case driverPhoto = "Driver Photo"
    case nextButton = "Next"
    case createAccountButton = "CREATE ACCOUNT"
    case driversLisense = "Driver's Lisense"
    case carLisensePlate = "Car License Plate"
    case carMakeOrModel = "Car Make/Model"
    case carImage = "Car image"
    case carInsurancePaperwork = "Upload car insurance paperwork"
    case driversLisenseImage = "Driver’s License Image"
    case carLisensePlateImage = "Car License Palte Image"
    case termsAndCondition = "I accept the terms & condition"
}

class SignUpDataModel {
    var identifier:     String?
    var items:          [SignUpCellDataItem]?
    
    
    
    init(identifier: String, items: [SignUpCellDataItem]) {
        self.identifier = identifier
        self.items      = items
    }
    
    
    class func getItemsForFirstStep() -> [SignUpDataModel] {
        
        return [SignUpDataModel(identifier: CellIdentifier.singleItemCell, items: [
                    SignUpCellDataItem(key: .firstName, title: InputFieldType.firstName.rawValue, value: "", placeholder: "", isSecure: false)]),
                
                SignUpDataModel(identifier: CellIdentifier.singleItemCell, items: [
                    SignUpCellDataItem(key: .lastName, title: InputFieldType.lastName.rawValue, value: "", placeholder: "", isSecure: false)]),
                
                SignUpDataModel(identifier: CellIdentifier.singleItemCell, items: [
                    SignUpCellDataItem(key: .email, title: InputFieldType.email.rawValue, value: "", placeholder: "", isSecure: false)]),
                
                SignUpDataModel(identifier: CellIdentifier.singleItemCell, items: [
                    SignUpCellDataItem(key: .password, title: InputFieldType.password.rawValue, value: "", placeholder: "", isSecure: true)]),
                
                SignUpDataModel(identifier: CellIdentifier.singleItemCell, items: [
                    SignUpCellDataItem(key: .confirmPassword, title: InputFieldType.confirmPassword.rawValue, value: "", placeholder: "", isSecure: true)]),
                
                SignUpDataModel(identifier: CellIdentifier.doubleItemCell, items: [
                    SignUpCellDataItem(key: .phoneNumber, title: InputFieldType.phoneNumber.rawValue, value: "", placeholder: "", isSecure: false),
                    SignUpCellDataItem(key: .dateOfBirth, title: InputFieldType.dateOfBirth.rawValue, value: "", placeholder: "", isSecure: false)]),
                
                SignUpDataModel(identifier: CellIdentifier.photoItemCell, items: [
                    SignUpCellDataItem(key: .driverPhoto, title: InputFieldType.driverPhoto.rawValue, value: "Choose File", placeholder: "", isSecure: false)]),
                
                SignUpDataModel(identifier: CellIdentifier.buttonItemCell, items: [
                    SignUpCellDataItem(key: .nextButton, title: InputFieldType.nextButton.rawValue, value: "", placeholder: "", isSecure: false)])
        ]
    }
    
    
    class func getItemsForSecondStep() -> [SignUpDataModel] {
        
        return [SignUpDataModel(identifier: CellIdentifier.singleItemCell, items: [
                    SignUpCellDataItem(key: .driversLisense, title: InputFieldType.driversLisense.rawValue, value: "", placeholder: "", isSecure: false)]),
                
                SignUpDataModel(identifier: CellIdentifier.multipleImageUploadCell, items: [
                    SignUpCellDataItem(key: .driversLisenseImage, title: InputFieldType.driversLisenseImage.rawValue, value: [], placeholder: "", isSecure: false)]),
                
                SignUpDataModel(identifier: CellIdentifier.singleItemCell, items: [
                    SignUpCellDataItem(key: .carLisensePlate, title: InputFieldType.carLisensePlate.rawValue, value: "", placeholder: "", isSecure: false)]),
                
                SignUpDataModel(identifier: CellIdentifier.multipleImageUploadCell, items: [
                    SignUpCellDataItem(key: .carLisensePlateImage, title: InputFieldType.carLisensePlateImage.rawValue, value: [], placeholder: "", isSecure: false)]),
                
                SignUpDataModel(identifier: CellIdentifier.singleItemCell, items: [
                    SignUpCellDataItem(key: .carMakeOrModel, title: InputFieldType.carMakeOrModel.rawValue, value: "", placeholder: "", isSecure: false)]),
                
                SignUpDataModel(identifier: CellIdentifier.photoItemCell, items: [
                    SignUpCellDataItem(key: .carImage, title: InputFieldType.carImage.rawValue, value: "Choose File", placeholder: "", isSecure: false)]),
                
                SignUpDataModel(identifier: CellIdentifier.photoItemCell, items: [
                    SignUpCellDataItem(key: .carInsurancePaperwork, title: InputFieldType.carInsurancePaperwork.rawValue, value: "Choose File", placeholder: "", isSecure: false)]),
                
                SignUpDataModel(identifier: CellIdentifier.termsAndConditionCell, items: [
                    SignUpCellDataItem(key: .termsAndCondition, title: InputFieldType.termsAndCondition.rawValue, value: false, placeholder: "", isSecure: false)]),
            
                SignUpDataModel(identifier: CellIdentifier.buttonItemCell, items: [
                    SignUpCellDataItem(key: .createAccountButton, title: InputFieldType.createAccountButton.rawValue, value: "", placeholder: "", isSecure: false)])
        ]
    }
    
}


struct SignUpCellDataItem{
    
    var key:            InputFieldType?
    var title:          String?
    var value:          Any?
    var placeholder:    String?
    var isSecure:       Bool?
}

