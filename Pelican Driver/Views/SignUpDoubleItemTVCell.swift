//
//  SignUpDoubleItemTVCell.swift
//  Pelican Driver
//
//  Created by iMac on 31/1/19.
//  Copyright © 2019 Silicon Orchard Ltd. All rights reserved.
//

import UIKit

class SignUpDoubleItemTVCell: UITableViewCell {

    @IBOutlet weak var firstTitle: UILabel!
    @IBOutlet weak var secondTitle: UILabel!
    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    
    func configCellWith(dataItem: SignUpDataModel) {
        self.firstTitle.text = dataItem.items?.first?.title
        self.secondTitle.text = dataItem.items?[1].title
        self.firstTextField.isSecureTextEntry = dataItem.items?[0].isSecure ?? false
        self.firstTextField.text = dataItem.items?[0].value as? String
        self.secondTextField.isSecureTextEntry = dataItem.items?[1].isSecure ?? false
        self.secondTextField.text = dataItem.items?[1].value as? String
    }
    
}
