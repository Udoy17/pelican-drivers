//
//  SignUpSingleItemTVC.swift
//  Pelican Driver
//
//  Created by iMac on 31/1/19.
//  Copyright © 2019 Silicon Orchard Ltd. All rights reserved.
//

import UIKit

class SignUpSingleItemTVCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var inputTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    
    func configCellWith(dataItem: SignUpDataModel) {
        self.titleLabel.text = dataItem.items?.first?.title
        self.inputTextField.isSecureTextEntry = dataItem.items?.first?.isSecure ?? false
        self.inputTextField.text = dataItem.items?.first?.value as? String
    }
    
}
