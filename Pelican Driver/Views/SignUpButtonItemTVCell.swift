//
//  SignUpButtonItemTVCell.swift
//  Pelican Driver
//
//  Created by iMac on 31/1/19.
//  Copyright © 2019 Silicon Orchard Ltd. All rights reserved.
//

import UIKit

class SignUpButtonItemTVCell: UITableViewCell {

    @IBOutlet weak var centerXConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextSignUpButton: UIButton!    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nextSignUpButton.doRounded(height: 12, border: false, color: UIColor.templateGreen.cgColor)
        nextSignUpButton.setTitleColor(UIColor.templateGreen, for: .normal)
        giveButtonShadow()
    }

    func giveButtonShadow() {
        nextSignUpButton.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        nextSignUpButton.layer.shadowOffset = CGSize(width: 0, height: 3)
        nextSignUpButton.layer.shadowOpacity = 1.0
        nextSignUpButton.layer.shadowRadius = 5.0
        nextSignUpButton.layer.masksToBounds = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func configCellWith(dataItem: SignUpDataModel) {
        self.nextSignUpButton.setTitle(dataItem.items?.first?.title, for: .normal)
        self.buttonWidthConstraint.constant = 200
        self.centerXConstraint.constant = 0
    }
    
}
