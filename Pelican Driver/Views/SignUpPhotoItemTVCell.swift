//
//  SignUpPhotoItemCell.swift
//  Pelican Driver
//
//  Created by iMac on 31/1/19.
//  Copyright © 2019 Silicon Orchard Ltd. All rights reserved.
//

import UIKit

class SignUpPhotoItemTVCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var chooseFileButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        chooseFileButton.doRounded(height: 15.0, border: true, color: UIColor.templateGreen.cgColor)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configCellWith(dataItem: SignUpDataModel) {
        self.titleLabel.text = dataItem.items?.first?.title
        self.chooseFileButton.setTitle(dataItem.items?.first?.value as? String, for: .normal)
    }
    
}
