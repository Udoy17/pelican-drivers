//
//  TermsAndConditionTVCell.swift
//  Pelican Driver
//
//  Created by iMac on 1/2/19.
//  Copyright © 2019 Silicon Orchard Ltd. All rights reserved.
//

import UIKit

class TermsAndConditionTVCell: UITableViewCell {

    @IBOutlet weak var termsAndConditionButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func configCellWith(dataItem: SignUpDataModel) {
        self.termsAndConditionButton.isSelected = dataItem.items?.first?.value as! Bool
    }
    
}
