//
//  MultipleImageUploadCell.swift
//  Pelican Driver
//
//  Created by iMac on 1/2/19.
//  Copyright © 2019 Silicon Orchard Ltd. All rights reserved.
//

import UIKit
import Foundation

class MultipleImageUploadCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBOutlet weak var imageTitleLabel: UILabel!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    var imagePicker: UIImagePickerController!
    
    var imageCellId = "imageCellId"
    var addImageCellId = "addImageCellId"
    var key: InputFieldType!
    var cellDataModel: SignUpDataModel!
    
    var driversLisenseImages = [UIImage]()
    var carLisensePlateImages = [UIImage]()
    
    var imageData = [UIImage]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        setupViews()
    }
    
    
    func setupViews() {
        setupCollectionView()
    }
    
    func setupCollectionView() {
        imageCollectionView.delegate = self
        imageCollectionView.dataSource = self
        imageCollectionView.register(ImageCell.self, forCellWithReuseIdentifier: imageCellId)
        imageCollectionView.register(AddImageCell.self, forCellWithReuseIdentifier: addImageCellId)
    }
    
    
    func configCellWith(dataItem: SignUpDataModel) {
        self.imageTitleLabel.text = dataItem.items?.first?.title
        self.cellDataModel = dataItem
        
        if let items = dataItem.items, let firstItem = items.first, let values = firstItem.value {
            if firstItem.key == .driversLisenseImage {
                self.key = .driversLisenseImage
                self.driversLisenseImages = values as! [UIImage]
            }else {
                self.key = .carLisensePlateImage
                self.carLisensePlateImages = values as! [UIImage]
            }
        }
        self.imageCollectionView.reloadData()
    }
    
    
    //MARK: - COLLECTION VIEW METHODS
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.key == .driversLisenseImage {
            return driversLisenseImages.count + 1
        } else if self.key == .carLisensePlateImage {
            return carLisensePlateImages.count + 1
        }
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var count = 0
        if self.key == .driversLisenseImage {
            count = driversLisenseImages.count
        } else if self.key == .carLisensePlateImage {
            count = carLisensePlateImages.count
        }
        
        if indexPath.row == count {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: addImageCellId, for: indexPath) as! AddImageCell
            if !cell.addButton.allTargets.contains(self) {
                cell.addButton.addTarget(self, action: #selector(addButtonClicked(sender:)), for: .touchUpInside)
            }
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: imageCellId, for: indexPath) as! ImageCell
        if self.key == .driversLisenseImage {
            cell.lisenseImageView.image = driversLisenseImages[indexPath.row]
        } else if self.key == .carLisensePlateImage {
            cell.lisenseImageView.image = carLisensePlateImages[indexPath.row]
        }
        return cell
    }
    
    
    //MARK: - CUSTOM METHODS
    @objc func addButtonClicked(sender: UIButton) {

        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let topVC = UIApplication.topViewController(controller: appDelegate.window?.rootViewController) {
            topVC.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func setImageToModel(images: [UIImage]) {
        if var items = cellDataModel.items, var item = items.first, var imageValues = item.value{
            imageValues = driversLisenseImages
            item.value = imageValues
            items = [item]
            cellDataModel.items = items
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            print("No image found")
            return
        }
        if self.key == .driversLisenseImage {
            driversLisenseImages.append(image)
            if var items = cellDataModel.items, var item = items.first, var imageValues = item.value{
                imageValues = driversLisenseImages
                item.value = imageValues
                items = [item]
                cellDataModel.items = items
            }
        } else if self.key == .carLisensePlateImage {
            carLisensePlateImages.append(image)
            if var items = cellDataModel.items, var item = items.first, var imageValues = item.value{
                imageValues = carLisensePlateImages
                item.value = imageValues
                items = [item]
                cellDataModel.items = items
            }
        }
        self.imageCollectionView.reloadData()
    }
}


class ImageCell: UICollectionViewCell {
    
    let lisenseImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "lisense")
        iv.contentMode = .scaleAspectFill
        iv.doCornerAndBorder(radius: 16.0, border: 1, color: UIColor.templateGreen.cgColor)
        iv.layer.masksToBounds = true
        return iv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented.")
    }
    
    func setupView() {
        self.addSubview(lisenseImageView)
        lisenseImageView.frame = CGRect(x: 0, y: 0, width: self.contentView.frame.height, height: self.contentView.frame.height)
    }
}

class AddImageCell: UICollectionViewCell {

    let addButton: UIButton = {
        let button = UIButton()
        button.setTitle("+", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 40)
        button.setTitleColor(UIColor.templateGreen, for: .normal)
        button.doCornerAndBorder(radius: 12.0, border: 2, color: UIColor.templateGreen.cgColor)
        return button
    }()


    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented.")
    }

    func setupView() {
        self.addSubview(addButton)
        addButton.frame = CGRect(x: 0, y: 0, width: self.contentView.frame.height, height: self.contentView.frame.height)
    }
}


protocol CaptureImageDelegate {
    func captureButtonTappedFor(indexPath: IndexPath)
}
