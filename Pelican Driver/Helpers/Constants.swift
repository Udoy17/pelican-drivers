//
//  Constants.swift
//  Pelican Driver
//
//  Created by iMac on 31/1/19.
//  Copyright © 2019 Silicon Orchard Ltd. All rights reserved.
//

import Foundation

struct Constants {
    static let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{3,64}"
}

struct CellIdentifier{
    static let singleItemCell = "singleItemCell"
    static let doubleItemCell = "doubleItemCell"
    static let photoItemCell = "photoItemCell"
    static let buttonItemCell = "buttonItemCell"
    static let multipleImageUploadCell = "multipleImageUploadCell"
    static let termsAndConditionCell = "termsAndConditionCell"
}
