//
//  Extensions.swift
//  Pelican Driver
//
//  Created by iMac on 30/1/19.
//  Copyright © 2019 Silicon Orchard Ltd. All rights reserved.
//

import UIKit


extension UIView {
    func doCornerAndBorder(radius:CGFloat,border:CGFloat,color: CGColor){
        self.layer.cornerRadius = radius
        self.layer.borderWidth = border
        self.layer.borderColor = color
        self.clipsToBounds = true
    }
    
    func doRounded(height:CGFloat, border: Bool, color: CGColor){
        if border {
            self.layer.borderColor = color
            self.layer.borderWidth = 1.0
        }
        self.layer.masksToBounds = false
        self.layer.cornerRadius = height/2
        self.clipsToBounds = true
        
    }
    
    func doBottomShadow() {
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.shadowOpacity = 1.0
        self.layer.shadowOffset = CGSize(width: 0.5, height: 4.0)
        self.layer.shadowRadius = 2.0
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 4.0
    }
}


extension UIColor {
    static var templateGreen: UIColor {
        return UIColor(red: 14/255, green: 125/255, blue: 64/255, alpha: 1.0)
    }
    static var templateDeepGreen: UIColor {
        return UIColor(red:0.22, green:0.48, blue:0.16, alpha:1.0)
    }
    static var warningBackground: UIColor {
        return UIColor(red: 255/255, green: 210/255, blue: 210/255, alpha: 1.0)
    }
}



extension UIApplication {
    
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
