//
//  ResetPasswordVC.swift
//  Pelican Driver
//
//  Created by iMac on 30/1/19.
//  Copyright © 2019 Silicon Orchard Ltd. All rights reserved.
//

import UIKit

class ResetPasswordVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var inputContainerView: UIView!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var code1TF: UITextField!
    @IBOutlet weak var code2TF: UITextField!
    @IBOutlet weak var code3TF: UITextField!
    @IBOutlet weak var code4TF: UITextField!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    
    
    var currentState = CurrentResetPasswordState.email
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        self.addGestureToHideKeyoardOnTappingAround()
    }
    
    
    //MARK: - SETUP UI
    func setupUI() {
        nextButton.backgroundColor = UIColor.templateGreen
        nextButton.doCornerAndBorder(radius: 6.0, border: 2.0, color: UIColor.templateGreen.cgColor)
        nextButton.setTitleColor(UIColor.white, for: .normal)
        setupTF()
    }
    
    
    func setupTF() {
        setupTextFieldTag()
        setupTextFieldDelegate()
    }
    
    
    func setupTextFieldTag(){
        emailTF.tag = 101
        code1TF.tag = 102
        code2TF.tag = 103
        code3TF.tag = 104
        code4TF.tag = 105
        newPasswordTF.tag = 106
        confirmPasswordTF.tag = 107
    }
    
    
    func setupTextFieldDelegate() {
        emailTF.delegate = self
        code1TF.delegate = self
        code2TF.delegate = self
        code3TF.delegate = self
        code4TF.delegate = self
        newPasswordTF.delegate = self
        confirmPasswordTF.delegate = self
    }
    
    
    //MARK: - BUTTON ACTIONS
    @IBAction func nextButtonTapped(_ sender: Any) {
        switch currentState {
        case .email:
//            self.handleEmailState()
            slideInputContainerView()
            currentState = .verificationCode
            break
        case .verificationCode:
            slideInputContainerView()
            currentState = .passwordSet
            break
        case .passwordSet:
            break
        }
    }
    
    
    @IBAction func codeChanged(_ sender: UITextField) {
//        if sender.text != "" && sender != code4TF{
//            if let nextTF = self.view.viewWithTag(sender.tag + 1) as? UITextField {
//                nextTF.becomeFirstResponder()
//            }
//            return
//        }
//        if sender != code1TF {
//            if let previousTF = self.view.viewWithTag(sender.tag - 1) as? UITextField {
//                previousTF.becomeFirstResponder()
//            }
//        }
    }
    
    //MARK: - CUSTOM METHODS
    func handleEmailState() {
        guard let email = getValidEmail(textField: emailTF) else {
            showAlert(title: "Invalid Email", message: "Please enter a valid email!!")
            return
        }
        print(email)
        //TODO: - Call API to send verification code
    }
    
    
    func handleVerificationCodeState() {
        guard let code1 = getValueOf(textField: code1TF), let code2 = getValueOf(textField: code2TF), let code3 = getValueOf(textField: code3TF), let code4 = getValueOf(textField: code4TF) else {
            showAlert(title: "Invalid Code", message: "Please enter valid varification code!!")
            return
        }
        print(code1)
        print(code2)
        print(code3)
        print(code4)
        //TODO: - Call API to check code
    }
    
    
    func handlePasswordSetState() {
        guard let newPassword = getValueOf(textField: newPasswordTF), let confirmedPassword = getValueOf(textField: confirmPasswordTF) else {
            showAlert(title: "Invalid Code", message: "Please enter valid password!!")
            return
        }
        print(newPassword)
        print(confirmedPassword)
        //TODO: - Call API to set new password
    }
    
    
    func getValueOf(textField: UITextField) -> String?{
        guard let value = textField.text, !value.trimmingCharacters(in: .whitespaces).isEmpty else {
            return nil
        }
        return value
    }
    
    
    func getValidEmail(textField: UITextField) -> String? {
        guard let email = getValueOf(textField: textField) else {
            return nil
        }
        let emailTest = NSPredicate(format:"SELF MATCHES %@", Constants.emailRegEx)
        if emailTest.evaluate(with: email) {
            return email
        }
        return nil
    }
    
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    
    func slideInputContainerView() {
        UIView.animate(withDuration: 0.5) {
            self.inputContainerView.frame.origin.x -= self.view.frame.width
        }
    }
    
    
    func addGestureToHideKeyoardOnTappingAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    
    //MARK: - UITextFieldDelegate METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 101 || textField.tag == 105 || textField.tag == 107 {
            hideKeyboard()
            return true
        }
        if let nextTF = self.view.viewWithTag(textField.tag + 1) as? UITextField {
            nextTF.becomeFirstResponder()
        }
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == code1TF || textField == code2TF || textField == code3TF || textField == code4TF {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

            return updatedText.count <= 1
        }
        return true
    }
}

enum CurrentResetPasswordState{
    case email
    case verificationCode
    case passwordSet
}
