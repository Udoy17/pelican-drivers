//
//  SecondSignUpVC.swift
//  Pelican Driver
//
//  Created by iMac on 31/1/19.
//  Copyright © 2019 Silicon Orchard Ltd. All rights reserved.
//

import UIKit

class SignUpSecondStepVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var signUpSecondStepTableView: UITableView!
    
    
    var termsAccepted = false
    let SignUpSecondStepItems: [SignUpDataModel] =  SignUpDataModel.getItemsForSecondStep()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationView()
        setupTableView()
        registerTVCell()
    }
    
    
    //MARK: - SETUP
    func setupNavigationView() {
        self.title = "SEGN UP"
        self.navigationController?.navigationBar.barTintColor = UIColor.templateGreen
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.white
    }
    
    
    func setupTableView() {
        signUpSecondStepTableView.delegate    = self
        signUpSecondStepTableView.dataSource  = self
        signUpSecondStepTableView.tableFooterView = UIView(frame: .zero)
    }

    
    func registerTVCell() {
        signUpSecondStepTableView.register(UINib(nibName: "SignUpSingleItemTVCell", bundle: nil), forCellReuseIdentifier: CellIdentifier.singleItemCell)
        signUpSecondStepTableView.register(UINib(nibName: "SignUpDoubleItemTVCell", bundle: nil), forCellReuseIdentifier: CellIdentifier.doubleItemCell)
        signUpSecondStepTableView.register(UINib(nibName: "SignUpPhotoItemTVCell", bundle: nil), forCellReuseIdentifier: CellIdentifier.photoItemCell)
        signUpSecondStepTableView.register(UINib(nibName: "SignUpButtonItemTVCell", bundle: nil), forCellReuseIdentifier: CellIdentifier.buttonItemCell)
        signUpSecondStepTableView.register(UINib(nibName: "MultipleImageUploadCell", bundle: nil), forCellReuseIdentifier: CellIdentifier.multipleImageUploadCell)
        signUpSecondStepTableView.register(UINib(nibName: "TermsAndConditionTVCell", bundle: nil), forCellReuseIdentifier: CellIdentifier.termsAndConditionCell)
    }
    
    
    //MARK: - BUTTON ACTIONS
    @objc func createAccountButtonClicked(sender: UIButton) {
        self.view.endEditing(true)
        //Validation
        print("Create Account")
    }
    
    @objc func termsAndConditionClicked(sender: UIButton) {
        for dataItem in SignUpSecondStepItems {
            if dataItem.identifier == CellIdentifier.termsAndConditionCell {
                var item = dataItem.items![0]
                let currentValue = item.value ?? false
                item.value = !(currentValue as! Bool)
                dataItem.items = [item]
                self.signUpSecondStepTableView.reloadData()
            }
        }
    }
    
    
    //MARK: - TABLEVIEW METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SignUpSecondStepItems.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dataItem = SignUpSecondStepItems[indexPath.row]
        switch dataItem.identifier{
            
        case CellIdentifier.singleItemCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.singleItemCell, for: indexPath) as! SignUpSingleItemTVCell
            cell.configCellWith(dataItem: dataItem)
            cell.inputTextField.delegate = self
            return cell
            
        case CellIdentifier.multipleImageUploadCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.multipleImageUploadCell, for: indexPath) as! MultipleImageUploadCell
            cell.configCellWith(dataItem: dataItem)
            return cell
            
        case CellIdentifier.photoItemCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.photoItemCell, for: indexPath) as! SignUpPhotoItemTVCell
            cell.configCellWith(dataItem: dataItem)
            return cell
            
        case CellIdentifier.buttonItemCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.buttonItemCell, for: indexPath) as! SignUpButtonItemTVCell
            cell.configCellWith(dataItem: dataItem)
            if !cell.nextSignUpButton.allTargets.contains(self) {
                cell.nextSignUpButton.addTarget(self, action: #selector(createAccountButtonClicked(sender:)), for: .touchUpInside)
            }
            return cell
            
        case CellIdentifier.termsAndConditionCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.termsAndConditionCell, for: indexPath) as! TermsAndConditionTVCell
            cell.configCellWith(dataItem: dataItem)
            if !cell.termsAndConditionButton.allTargets.contains(self) {
                cell.termsAndConditionButton.addTarget(self, action: #selector(termsAndConditionClicked(sender:)), for: .touchUpInside)
            }
            return cell
            
        default:
            break
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == SignUpSecondStepItems.count - 2 {
            return 40
        } else if indexPath.row == SignUpSecondStepItems.count - 1 {
            return 60
        }
        return 100
    }
}


extension SignUpSecondStepVC: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let textFieldPosition: CGPoint = textField.convert(CGPoint.zero, to: self.signUpSecondStepTableView)
        let indexPath = self.signUpSecondStepTableView.indexPathForRow(at: textFieldPosition)
        
        if let indexPath = indexPath {
            let model = self.SignUpSecondStepItems[indexPath.row]
            var item = model.items![0]
            item.value = textField.text ?? ""
            model.items = [item]
        }
        self.signUpSecondStepTableView.reloadData()
    }
}
